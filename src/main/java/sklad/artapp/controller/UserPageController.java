package sklad.artapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserPageController {

    @GetMapping(path = "/userpage")
    public String loadUserPage() {
        return "userpage";
    }

    @GetMapping(path="/logout")
    public String logout() { return "hello";}
}
