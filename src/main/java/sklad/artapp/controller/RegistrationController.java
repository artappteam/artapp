package sklad.artapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import sklad.artapp.model.*;
import sklad.artapp.service.ArtAppUserService;
import sklad.artapp.service.ArtistUserService;
import sklad.artapp.service.CollectorUserService;

import java.util.Optional;

@Controller
public class RegistrationController {

   /* private ArtAppUserService artAppUserService;

    public RegistrationController(ArtAppUserService artAppUserService) {
        this.artAppUserService = artAppUserService;
    }*/

    private ArtistUserService artistUserService;
    private CollectorUserService collectorUserService;

    public RegistrationController(ArtistUserService artistUserService, CollectorUserService collectorUserService) {
        this.artistUserService = artistUserService;
        this.collectorUserService = collectorUserService;
    }

    @GetMapping(path = "/registration")
    public String addUserForm(Model model, ArtAppUser artAppUser) {

        model.addAttribute("artAppUserForm", new ArtAppUserForm());

        return "registration";
    }

    @PostMapping("/registration")
    public String addNewUser(@ModelAttribute ArtAppUserForm artAppUserForm,
                             Model model) {
        if (artAppUserForm.getRole().equals(Role.ARTIST)) {
            Optional<String> errorMessage = artistUserService.createUser(artAppUserForm);
            if (errorMessage.isPresent()) {
                model.addAttribute("errorMessage", errorMessage.get());
                return "registration";
            }
        } else {
            Optional<String> errorMessage = collectorUserService.createUser(artAppUserForm);
            if (errorMessage.isPresent()) {
                model.addAttribute("errorMessage", errorMessage.get());
                return "registration";
            }
        }

        return "hello";
    }

/*
    @GetMapping(path = "/registration")
    public String addUserForm(Model model) {
        model.addAttribute("artAppUserForm", new ArtAppUserForm());
        return "registration";
    }


    @PostMapping("/registration")
    public String addNewUser(@ModelAttribute ArtAppUserForm artAppUserForm, BindingResult bindingResult,
                             Model model) {
        Optional<String> errorMessage = artAppUserService.createUser(artAppUserForm);

        if (errorMessage.isPresent()) {
            model.addAttribute("errorMessage", errorMessage.get());
            return "registration";
        }

        return "hello";
    }*/
}
