package sklad.artapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sklad.artapp.model.Collector;

public interface CollectorUserRepository extends JpaRepository<Collector, Long> {



}
