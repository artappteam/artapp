package sklad.artapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sklad.artapp.model.ArtAppUser;
import sklad.artapp.model.Artist;

public interface ArtistUserRepository extends JpaRepository<Artist, Long> {

}
