package sklad.artapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sklad.artapp.model.Role;
import sklad.artapp.model.ArtAppUser;

import java.util.Optional;

@Repository
public interface ArtAppUserRepository extends JpaRepository<ArtAppUser, Long> {
    Optional<ArtAppUser> findByFirstName(String firstName);
    Optional<ArtAppUser> findByLastName(String lastName);
    Optional<ArtAppUser> findByEmail(String email);
    Optional<ArtAppUser> findByRole(Role role);
    Optional<ArtAppUser> findByUsername(String username);
}
