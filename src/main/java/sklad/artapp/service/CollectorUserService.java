package sklad.artapp.service;

import org.springframework.stereotype.Service;
import sklad.artapp.model.*;
import sklad.artapp.repository.ArtAppUserRepository;
import sklad.artapp.repository.ArtistUserRepository;
import sklad.artapp.repository.CollectorUserRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Service
public class CollectorUserService extends ArtAppUserService {

    private CollectorUserRepository collectorUserRepository;


    public CollectorUserService(ArtAppUserRepository userRepository, CollectorUserRepository collectorUserRepository) {
        super(userRepository);
        this.collectorUserRepository = collectorUserRepository;
    }

    public Optional<String> createUser(ArtAppUserForm collectorForm) {
        Optional<String> errors = validateFormData(collectorForm);
        if (errors.isPresent()) {
            return errors;
        }
        collectorUserRepository.save(toCollectorUser(collectorForm));
        return Optional.empty();
    }

    private Collector toCollectorUser(ArtAppUserForm collectorForm) {
        Collector collector = new Collector();
        super.toArtAppUser(collector, collectorForm);
        collector.setRole(collectorForm.getRole());
        collector.setTypesOfArt(collectorForm.getCollectorForm().getTypesOfArt());
        return collector;
    }

    private Optional<String> validateFormDataForCollector(ArtAppUserForm collectorForm) {
        super.validateFormData(collectorForm);
        return Optional.empty();
    }
}

