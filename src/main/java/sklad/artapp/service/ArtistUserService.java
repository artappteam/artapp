package sklad.artapp.service;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import sklad.artapp.model.ArtAppUser;
import sklad.artapp.model.ArtAppUserForm;
import sklad.artapp.model.Artist;
import sklad.artapp.model.ArtistForm;
import sklad.artapp.repository.ArtAppUserRepository;
import sklad.artapp.repository.ArtistUserRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class ArtistUserService extends ArtAppUserService {

    private ArtistUserRepository artistUserRepository;


    public ArtistUserService(ArtAppUserRepository userRepository, ArtistUserRepository artistUserRepository) {
        super(userRepository);
        this.artistUserRepository = artistUserRepository;
    }

    public Optional<String> createUser(ArtAppUserForm artistForm) {
        Optional<String> errors = validateFormData(artistForm);
        if (errors.isPresent()) {
            return errors;
        }
        artistUserRepository.save(toArtistUser(artistForm));
        return Optional.empty();
    }

    private Artist toArtistUser(ArtAppUserForm artistForm) {
        Artist artist = new Artist();
        super.toArtAppUser(artist, artistForm);
        artist.setAcademy(artistForm.getArtistForm().getAcademy());
        artist.setCityOfBirth(artistForm.getArtistForm().getCityOfBirth());
        artist.setDateOfBirth(artistForm.getArtistForm().getDateOfBirth());
        artist.setDescription(artistForm.getArtistForm().getDescription());
        return artist;
    }

    private Optional<String> validateFormDataForArtist(ArtAppUserForm artistForm) {
        super.validateFormData(artistForm);

        if (!validDateOfBirth(artistForm.getArtistForm().getDateOfBirth())) {
            return Optional.of("Invalid date of birth");
        }
        return Optional.empty();
    }

    private boolean validDateOfBirth(LocalDate dateOfBirth) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        return dateOfBirth.isBefore(LocalDate.now());
    }
}
