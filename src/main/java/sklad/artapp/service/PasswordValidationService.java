package sklad.artapp.service;

import java.nio.charset.StandardCharsets;

public class PasswordValidationService {


    public static boolean ifCorrectPassword(String password) {
        byte[] asciiBytes = password.getBytes(StandardCharsets.US_ASCII);
        boolean x = false, y = false, z = false;

        for (int i = 0; i < asciiBytes.length; i++) {
            if (ifSpecialSigns(asciiBytes[i])) {
                x = true;
            }
        }

        for (int i = 0; i < asciiBytes.length; i++) {
            if (ifDigits(asciiBytes[i])) {
                y = true;
            }
        }

        for (int i = 0; i < asciiBytes.length; i++) {
            if (ifBigLetters(asciiBytes[i])) {
                z = true;
            }
        }

        return (x && y && z);

    }

    private static boolean ifSpecialSigns(byte b) {

        if ((b >= 33 && b <= 47) || (b >= 58 && b <= 64) ||
                (b >= 91 && b <= 96) || (b >= 123 && b <= 126)) {
                    return true;
        }

        return false;
    }


    private static boolean ifDigits(byte b) {

        if ((b >= 48 && b <= 57)) {
                    return true;
        }

        return false;
    }


    private static boolean ifBigLetters(byte b) {

        if ((b >= 65 && b <= 90)) {
                    return true;
        }

        return false;
    }


}
