package sklad.artapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import sklad.artapp.model.ArtAppUser;
import sklad.artapp.repository.ArtAppUserRepository;

import java.util.Collections;
import java.util.NoSuchElementException;

@Service
public class ArtAppUserDetailsService implements UserDetailsService {

    @Autowired
    private ArtAppUserRepository userRepository;

    private UserDetails toUserDetails(ArtAppUser user) {
        return User.builder()
                .username(user.getUsername())
                .password(user.getPassword())
                .authorities(Collections.EMPTY_LIST)
                .accountExpired(false)
                .accountLocked(false)
                .credentialsExpired(false)
                .build();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username)
                .map(this::toUserDetails)
                .orElseThrow(NoSuchElementException::new);
    }
}
