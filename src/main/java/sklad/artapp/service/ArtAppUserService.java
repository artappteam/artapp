package sklad.artapp.service;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import sklad.artapp.model.ArtAppUser;
import sklad.artapp.model.ArtAppUserForm;
import sklad.artapp.repository.ArtAppUserRepository;
import org.apache.commons.validator.routines.EmailValidator;

import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Optional;


public class ArtAppUserService {

    private ArtAppUserRepository userRepository;
    private EmailValidator emailValidator = EmailValidator.getInstance();

    public ArtAppUserService(ArtAppUserRepository userRepository) {
        this.userRepository = userRepository;
    }


    protected void toArtAppUser(ArtAppUser user,ArtAppUserForm artAppUserForm) {
        user.setUsername(artAppUserForm.getUsername());
        user.setFirstName(artAppUserForm.getFirstName());
        user.setLastName(artAppUserForm.getLastName());
        user.setRole(artAppUserForm.getRole());
        user.setEmail(artAppUserForm.getEmail());
        user.setPassword(artAppUserForm.getPassword());
    }


    protected Optional<String> validateFormData(ArtAppUserForm artAppUserForm) {

        if (!validUsername(artAppUserForm.getUsername())) {
            return Optional.of("Invalid username");
        }

        if (!validFirstName(artAppUserForm.getFirstName())) {
            return Optional.of("Invalid first name");
        }

        if (!validLastName(artAppUserForm.getLastName())) {
            return Optional.of("Invalid last name");
        }

        if (!validEmail(artAppUserForm.getEmail())) {
            return Optional.of("Invalid email");
        }

        if (!validPassword(artAppUserForm.getPassword())) {
            return Optional.of("Invalid password");
        }

        return Optional.empty();
    }

    private boolean validPassword(String password) {
        return PasswordValidationService.ifCorrectPassword(password);
    }



    private boolean validUsername(String username) {
        username = username.trim();
        return !username.equals("")
                && !userRepository.findByFirstName(username).isPresent();
    }

    private boolean validFirstName(String name) {
        name = name.trim();
        return !name.equals("")
                && !userRepository.findByFirstName(name).isPresent();
    }

    private boolean validLastName(String name) {
        name = name.trim();
        return !name.equals("")
                && !userRepository.findByLastName(name).isPresent();
    }

    private boolean validEmail(String email) {
        return emailValidator.isValid(email)
                && !userRepository.findByEmail(email).isPresent();
    }


}
