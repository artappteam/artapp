package sklad.artapp.model;

public enum TypesOfArt {

    SCULPTURE,
    PAINTING,
    PHOTOGRAPHY,
    ARCHITECTURE,
    INSTALATIONS;

}
