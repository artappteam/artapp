package sklad.artapp.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table
public class Photo {

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    @ManyToOne
    private Artist author;
    private String title;
    private String dimensions;
    private LocalDate year;
    private String technique;

}
