package sklad.artapp.model;


import lombok.Data;
import lombok.NoArgsConstructor;
import sklad.artapp.model.Role;

import javax.persistence.*;
import java.util.List;

@Inheritance(strategy = InheritanceType.JOINED)
@Entity
@Table
@Data
@NoArgsConstructor
public class ArtAppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String username;
    String firstName;
    String lastName;
    String email;
    String password;
    @Enumerated(EnumType.STRING)
    Role role;
    @OneToMany
    List<Photo> photos;

}
