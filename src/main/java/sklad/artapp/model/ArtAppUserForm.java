package sklad.artapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArtAppUserForm {

    private String username;
    private String firstName;
    private String lastName;
    private String password;
    private Role role;
    private String email;
    private ArtistForm artistForm;
    private CollectorForm collectorForm;
}
