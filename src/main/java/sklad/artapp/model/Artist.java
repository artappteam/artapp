package sklad.artapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Table;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@javax.persistence.Table
public class Artist extends ArtAppUser {

    private Role role = Role.ARTIST;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfBirth;

    // jeśli chcemyskorzystać z LocalDateTime to w ten sposób:
    // @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    // private LocalDateTime dateOfBirth;

    private String cityOfBirth;
    private String academy;
    private String description;

}
