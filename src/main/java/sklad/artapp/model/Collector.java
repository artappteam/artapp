package sklad.artapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class Collector extends ArtAppUser {


    private Role role = Role.COLLECTOR;
    private TypesOfArt typesOfArt;
}
