package sklad.artapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EntityScan(basePackages = {"sklad.artapp.model"})
public class ArtappApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArtappApplication.class, args);
    }
}
